public class TS4_MultiSelectPicklist_CTR {
    @AuraEnabled
    public static List<Asset> getAssets(){
        return [SELECT Id,Name FROM Asset WHERE AccountId IN (SELECT AccountId FROM Opportunity WHERE Id = '006f400000LBD5eAAH')];
    }
}