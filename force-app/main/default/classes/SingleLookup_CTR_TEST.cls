@isTest
public class SingleLookup_CTR_TEST {
    static testMethod void main(){
        Account account = new Account();
        account.Name = 'Macondo';
        insert account;
        SingleLookup_CTR.fetchLookUpValues('Macondo', 'SELECT Id,Name FROM Account', false, 'Name');
        SingleLookup_CTR.fetchLookUpValues('Macondo', 'SELECT Id,Name FROM Account WHERE Name = \'Macondo\'', true, 'Name');
    }
}