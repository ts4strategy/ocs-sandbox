@isTest
public class TS4_MultiLookup_CTR_Test {
    static testMethod void main(){
        Account account = new Account();
        account.Name = 'Macondo';
        insert account;
        
        Account account2 = new Account();
        account2.Name = 'Zenda';
        insert account2;

        Catalogo__c catalogo1 = new Catalogo__c();
        catalogo1.Name = 'Agua';
        catalogo1.Id__c = 3;
        catalogo1.TipoCatalogo__c = 'Operación';
        
        insert catalogo1;
        
        Catalogo__c catalogo2 = new Catalogo__c();
        catalogo2.Name = 'Water Logic';
        catalogo2.Id__c = 4;
        catalogo2.TipoCatalogo__c = 'Marca';
        
        Product2 product = new Product2();
        product.Name = 'WL2FW Anual $1,000.00';
        product.ProductCode = '387';
        product.Operacion__c = catalogo1.Id;
        product.ServicioTipo__c = catalogo2.Id;
        product.IsActive = true;
        insert product;

        
        Product2 product1 = new Product2();
        product1.Name = 'Maquiana $1,000.00';
        product1.ProductCode = '387';
        product1.Operacion__c = catalogo1.Id;
        product1.ServicioTipo__c = catalogo2.Id;
        product1.IsActive = true;
        insert product1;
        
        //product.RecordTypeId = Sc
        //product.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Servicio').getRecordTypeId();
        
        
        PricebookEntry pricebookEntry = new PricebookEntry(); 
        pricebookEntry.IsActive = true;
        pricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
        pricebookEntry.Product2Id = product.Id;
        pricebookEntry.UnitPrice = 1000;
        
        insert pricebookEntry;
                
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'Da igual porque se sustituye';
        opportunity.AccountId = account.Id;
        opportunity.CloseDate = System.today().addMonths(1);
        opportunity.Type = 'Negocio existente';
        opportunity.StageName = 'Identificación de necesidades';
        
        insert opportunity;
        
        Quote quotes = new Quote();
        quotes.Name = 'Da igual porque se sustituye';
        quotes.OpportunityId = opportunity.Id;
        quotes.Pricebook2Id = Test.getStandardPricebookId();
        quotes.Sucursal__c = catalogo2.Id;

        insert quotes;

		QuoteLineItem qlineitem = new QuoteLineitem ();
        qlineitem.QuoteId  = quotes.Id;
        qlineitem.Product2Id = product.Id;
        qlineitem.PricebookEntryId = pricebookEntry.Id;
        qlineitem.UnitPrice = pricebookEntry.UnitPrice;
        qlineitem.Quantity = 1;
		insert qlineitem;

        TS4_Productos_relacionados__c producrelacionado = new TS4_Productos_relacionados__c ();
        producrelacionado.TS4_Producto__c = product.Id;
        producrelacionado.TS4_Tipo_QuoteLine__c= 'Maquina';
        
        insert producrelacionado;
        
        List<sObject> excludeList = new List<sObject>();
        excludeList.add(account2);
            
        TS4_MultiLookup_CTR.getInitDataService(qlineitem.Id);
        
        TS4_MultiLookup_CTR.fetchLookUpValues('Macondo', 'SELECT Id,Name FROM Account', excludeList, false);
        TS4_MultiLookup_CTR.fetchLookUpValues('Macondo', 'SELECT Id,Name FROM Account WHERE Name = \'Macondo\'', new List<sObject>(), true);
        
        TS4_MultiLookup_CTR.fetchPicklistValues('SELECT Id,Name FROM Account', excludeList, false);
        TS4_MultiLookup_CTR.fetchPicklistValues('SELECT Id,Name FROM Account WHERE Name = \'Macondo\'', excludeList, true);
    }
}