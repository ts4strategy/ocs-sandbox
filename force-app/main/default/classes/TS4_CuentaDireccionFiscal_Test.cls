@isTest
public class TS4_CuentaDireccionFiscal_Test {
    static testMethod void getandInsertAddress() {
        Account account = new Account();
        account.Name = 'Macondo';
        insert account;
        TS4_Direcciones__c address = new TS4_Direcciones__c();
        address.TS4_Calle__c = 'Amarillo';
        address.TS4_NumeroExterior__c = '33';
        address.TS4_EntreCalles__c = 'Azul y Verde';
        address.Cuenta__c = account.Id;
        address.TS4_TipoDireccion__c = 'Dirección Fiscal';
        
        insert address;
        TS4_CuentaDireccionFiscal.searchAccount(account.Id);
        TS4_CuentaDireccionFiscal.saveAccount(address);   
    }
}