public class MultiLookup_CTR {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String queryString, List<sObject> ExcludeitemsList) {
        String searchKey = '%' + searchKeyWord + '%';
        List < sObject > returnList = new List < sObject > ();
        
        List<string> lstExcludeitems = new List<string>();
        for(sObject item : ExcludeitemsList ){
            lstExcludeitems.add(item.id);
        }
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records
        String sQuery = queryString + ' WHERE Name LIKE :searchKey AND Id NOT IN : lstExcludeitems ORDER BY createdDate DESC LIMIT 5';
        //String sQuery =  'SELECT id, Name FROM ' +ObjectName + ' WHERE Name LIKE :searchKey AND Id NOT IN : lstExcludeitems ORDER BY createdDate DESC LIMIT 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static List < sObject > fetchPicklistValues(String queryString, List<sObject> ExcludeitemsList, Boolean queryFilter) {
        List < sObject > returnList = new List < sObject > ();
        
        List<string> lstExcludeitems = new List<string>();
        for(sObject item : ExcludeitemsList ){
            lstExcludeitems.add(item.id);
        }
        
        //Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records
        String sQuery = '';
        if(queryFilter)
            sQuery = queryString + ' AND Id NOT IN : lstExcludeitems ORDER BY createdDate DESC LIMIT 5';
        else
            sQuery = queryString + ' WHERE Id NOT IN : lstExcludeitems ORDER BY createdDate DESC LIMIT 5';
        
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}