({
    doInit : function(component, event, helper){
        helper.searchHelper(component,event);
    },
    
    onblur : function(component,event,helper){
        // on mouse leave clear the listOfSeachRecords & hide the search result component
        component.set("v.listOfSearchRecords", null );
        component.set("v.SearchKeyWord", '');
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    onfocus : function(component,event,helper){
        // show the spinner,show child search result component and call helper function
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        component.set("v.listOfSearchRecords", null );
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        helper.searchHelper(component,event);
    },

    // function for clear the Record Selaction
    clear :function(component,event,heplper){
        var selectedPillId = event.getSource().get("v.name");
        var AllPillsList = component.get("v.lstSelectedRecords");

        for(var i = 0; i < AllPillsList.length; i++){
            if(AllPillsList[i].Id == selectedPillId){
                AllPillsList.splice(i, 1);
                component.set("v.lstSelectedRecords", AllPillsList);
            }
        }
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
    },
    
    clearAll : function(cmp, evn, hlp){
        let pillTarget = cmp.find("selectpill");
        
        if(Array.isArray(pillTarget)){
            pillTarget.forEach(function(element){
                $A.util.addClass(element, 'slds-hide');
            });
        }else{
            $A.util.addClass(pillTarget, 'slds-hide');
        }
        
        
        cmp.set("v.SearchKeyWord",null);
        cmp.set("v.listOfSearchRecords", null );
        cmp.set("v.lstSelectedRecords", []);
    },

    // This function call when the end User Select any record from the result list.
    handleComponentEvent : function(component, event, helper) {
        component.set("v.SearchKeyWord",null);
        // get the selected object record from the COMPONENT event
        var listSelectedItems =  component.get("v.lstSelectedRecords");
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedAccountGetFromEvent);
        component.set("v.lstSelectedRecords" , listSelectedItems);

        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');

        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },    
})