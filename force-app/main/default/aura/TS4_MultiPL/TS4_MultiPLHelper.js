({
    updateLists : function(component){
        if(component.get("v.lstSelectedRecords").length > 0){
            let rTotal = component.get("v.listOfSearchRecords");
            let rSlt = component.get("v.lstSelectedRecords");
            
            for(var i = 0; i < rSlt.length; i++){
                for(var j = 0; j < rTotal.length; j++){
                    if(rTotal[j].Id == rSlt[i].Id){
                        rTotal.splice(j, 1);
                        component.set("v.listOfSearchRecords",rTotal);
                    }
                }
            }
        }
    },
    
    searchHelper : function(component,event) {
        var action = component.get("c.fetchPicklistValues");
        action.setParams({
            'queryString' : component.get("v.queryString"),
            'ExcludeitemsList' : component.get("v.lstSelectedRecords"),
            'queryFilter' :  component.get("v.queryFilter")
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Records Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No se encontraron registros...');
                } else {
                    component.set("v.Message", '');
                    // set searchResult list with return value from server.
                }
                let mostrar = [];
                component.set("v.listOfSearchRecords", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
})