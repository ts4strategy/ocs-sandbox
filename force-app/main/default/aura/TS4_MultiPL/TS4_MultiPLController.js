({
    daniel : function(component, evn, hlp){
        hlp.updateLists(component);
    },
    
    doInit : function(component, event, helper){
        helper.searchHelper(component,event);
    },
    
    onfocus : function(component,event,helper){
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        var forOpen = component.find("searchRes");
       	$A.util.addClass(forOpen, 'slds-is-open');
       	$A.util.removeClass(forOpen, 'slds-is-close');
        helper.updateLists(component);
        console.log('---seleccionados  ' + component.get("v.lstSelectedRecords").length);
        console.log('---Total  ' + component.get("v.listOfSearchRecords").length);
    },
    
    onblur : function(component,event,helper){
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    // function for clear the Record Selaction
    clear :function(component,event,helper){
        var selectedPillId = event.getSource().get("v.name");
        var rSlt = component.get("v.lstSelectedRecords");
        var rTotal = component.get('v.listOfSearchRecords');
        helper.updateLists(component);
        for(var i = 0; i < rSlt.length; i++){
            if(rSlt[i].Id == selectedPillId){
                rTotal.push(rSlt[i]);
                rSlt.splice(i, 1);
                component.set("v.lstSelectedRecords", rSlt);
                component.set("v.listOfSearchRecords", rTotal );
            }
        }
    },
    
    clearAll : function(cmp, evn, hlp){
        let pillTarget = cmp.find("selectpill");
        if(Array.isArray(pillTarget)){
            pillTarget.forEach(function(element){
                $A.util.addClass(element, 'slds-hide');
            });
        }else{
            $A.util.addClass(pillTarget, 'slds-hide');
        }
        cmp.set("v.lstSelectedRecords", []);
    },
     
    selectR : function(component, event, helper){
        let title = event.getSource().get("v.name");
        var listSelectedItems =  component.get("v.lstSelectedRecords");
        var selectedAccountGetFromEvent = title[0];
	  
        listSelectedItems.push(selectedAccountGetFromEvent);
        component.set("v.lstSelectedRecords" , listSelectedItems);
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');

        component.set("v.isOpen", false);
    },
    
    
    handleComponentEvent : function(component, event, helper) {
        var listSelectedItems =  component.get("v.lstSelectedRecords");
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
	    
        listSelectedItems.push(selectedAccountGetFromEvent);
        component.set("v.lstSelectedRecords" , listSelectedItems);
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    editMode : function(component, event, helper) {
        component.set("v.Message", null);
        var listSelectedItems =  component.get("v.lstSelectedRecords");
        component.set("v.lstSelectedRecords" , listSelectedItems);
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    openModel: function(component, event, helper) {
        let modal = event.getSource().get("v.name");
        let indxdt = modal.replace('modal', '');
        let title = event.getSource().get("v.title");
        component.set("v.accboton", title);
        var action = component.get("c.getInitDataService");
        action.setParams({ quoteliId: title.Id });  
        action.setCallback(this, function(result){
            let resQlin= result.getReturnValue();
            let mapproduct = [];
            let mapconsumible = [];
            component.set("v.registroconsu", false); 
            component.set("v.registro", false); 
            for(var x = 0; x < resQlin.length; x++){
                if(resQlin[x].TS4_Tipo_QuoteLine__c =="Maquina"){
                    component.set("v.lstmaquina",resQlin[x]);
                }else{
                    if(resQlin[x].TS4_Tipo_QuoteLine__c =="Producto"){
                        component.set("v.registro", true); 
                        mapproduct.push(resQlin[x])
                    }else{
                        component.set("v.registroconsu", true); 
                        mapconsumible.push(resQlin[x])
                    }
                }
                component.set("v.lstprodconfig",mapproduct);
                component.set("v.lstaccesorio",mapconsumible);
            }
        });
        $A.enqueueAction(action);
		component.set("v.modal", title.Product2.Name);
		component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
})