({
    validateFields : function(cmp){
        let sembrado = cmp.get('v.record').rltOpp.StageName == 'Sembrado'? true: false;
        let tmpObj = [];        
        cmp.find('sectionAField').forEach(function(element){
            if(!element.get("v.value")){ tmpObj.push(element.get("v.label")); }
        });
        if(!sembrado){
            cmp.find('sectionCField').forEach(function(element){ 
                if(!element.get("v.value")){ tmpObj.push(element.get("v.label")); }
            });
            
            if(cmp.get("v.lookupOwner").Id == undefined){ tmpObj.push(cmp.find("LkOppOwner").get("v.label")); }
            if(cmp.get("v.lookupContactsCP").length == 0){ tmpObj.push(cmp.find("LookupCP").get("v.label")); }
            
            cmp.find('sectionDField').forEach(function(element){
                if(!element.get("v.value")){ tmpObj.push(element.get("v.label")); }
            });
        }
        if(cmp.get("v.newDataTable").length == 0){ tmpObj.push("Contactos y dirección de instalación"); }
        return tmpObj;
        
    },
    
    updAccount : function(cmp){
        let updateAccount = cmp.get("c.upAccount");
        updateAccount.setParams({"acc" : cmp.get("v.record").cuenta});
        this.promesa(cmp, updateAccount)
        .then($A.getCallback(function(){
        }))
        .catch($A.getCallback(function(error){
        }));
    },
    
    savePContacts : function(cmp){
        const savePContacts_act = cmp.get("c.savePContacts");
        savePContacts_act.setParams({rltContacts : cmp.get("v.lookupContactsCP"), con : cmp.get("v.Contrato")});
        
        this.promesa(cmp, savePContacts_act)
        .then($A.getCallback(function(response){
        }))
        .catch($A.getCallback(function(error){ console.log(error); }));
    },
    
    updContract : function(cmp){
        let contractId = cmp.get("v.record").contrato.Id;
        let contract = cmp.get("v.Contrato");
        contract.Id = contractId;
        contract.OwnerId = cmp.get("v.lookupOwner").Id;
        contract.TS4_Referido__c = cmp.get("v.lookupReference").Id;
        const save_act = cmp.get("c.upContract");
        save_act.setParams({"contrato" :  cmp.get("v.Contrato") });
        this.promesa(cmp, save_act)
        .then($A.getCallback(function(response){
        }))
        .catch($A.getCallback(function(error){ console.log(error); }));
    },
    
    saveAddress : function(cmp){
        let updAddress = [];
        cmp.get("v.newDataTable").forEach(function(address){
            address.TS4_Contrato__c = cmp.get("v.Contrato").Id;
            updAddress.push(address);
        });
        
        const saveAddress_act = cmp.get("c.saveAddress");
        saveAddress_act.setParams({adds : updAddress, con : cmp.get("v.Contrato") });
        this.promesa(cmp, saveAddress_act)
        .then($A.getCallback(function(response){
        }))
        .catch($A.getCallback(function(error){ console.log(error); }));
    },
    
    saveAssets : function(cmp){
        let updAssets = [];
        cmp.get("v.newDataTable").forEach(function(address){
            address.assets.forEach(function(asset){
                asset.TS4_Direccion__c = address.Id;
                updAssets.push(asset);
            });
        });
        let updDireccion = '';
        for(var a = 0;  a < updAssets.length; a++){
            let e = a +1;
            if(e == updAssets.length){
                updDireccion += updAssets[a].TS4_Direccion__c;
            }
            else{
                updDireccion += updAssets[a].TS4_Direccion__c + ',';                           
            }
        }
        const saveAssets_act = cmp.get("c.saveAssets");
        saveAssets_act.setParams({con : cmp.get("v.Contrato"), idQuoteLI : updAssets, direcciones : updDireccion});
        this.promesa(cmp, saveAssets_act)
        .then($A.getCallback(function(response){
        }))
        .catch($A.getCallback(function(error){ console.log(error); }));
    },
    
    saveIC : function(cmp){
        let IContact = [];
        cmp.get("v.newDataTable").forEach(function(address){
            address.instalationContacts.forEach(function(contact){
                let temp = {};
                temp.Name = 'Instalación - ' + contact.Name;
                temp.TS4_Contacto__c = contact.Id;
                temp.TS4_Contrato__c = cmp.get("v.Contrato").Id;
                temp.TS4_RolContacto_Pt__c = 'Instalación';
                temp.TS4_Direccion__c = address.Id;
                IContact.push(temp);
            });
        });
        const saveIC = cmp.get("c.saveIContacts");
        saveIC.setParams({rltContacts : IContact, con : cmp.get("v.Contrato") });
        this.promesa(cmp, saveIC)    
        .then($A.getCallback(function(response){
        }))
        .catch($A.getCallback(function(error){ console.log(error); }));
    },
    
    tableIC : function(cmp, sltAssets, sltAddress, sltContacts, table){
        sltAddress.assets = [];
        sltAddress.instalationContacts = [];
        sltAssets.forEach(function(asset){ sltAddress.assets.push(asset); });
        sltContacts.forEach(function(contact){
            contact.type = 'Instalación'; 
            sltAddress.instalationContacts.push(contact); 
        });
        return sltAddress;
    },
    
    removeIndexByKey: function(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                array.splice(i, 1);
            }
        }
        return array;
    },
    
    fillTableIContacts : function(cmp){
        let table = cmp.get("v.newDataTable");
        let a = 0;
        cmp.get("v.record").rltInstalationDirContac.forEach(function(address){
            address.instalationContacts = [];
            address.ContactosRelacionados__r.forEach(function(contact){
                address.instalationContacts.push(contact.TS4_Contacto__r);
            });  
            address.assets = [];
            let listprod= cmp.get("v.record").rltInstalationqli;
            for(var s = 0; s < listprod.length;s++){
                if(address.Id == listprod[s].TS4_Direcciones__c){
                 	console.log('Igual')
                    address.assets.push(listprod[s]);
                }                
            }
            a++;
            table.push(address);
        });
        cmp.set("v.newDataTable", table);
    },
    
    promesa : function(cmp, action){
        return new Promise( function(resolve, reject){
            action.setCallback(this, function(response){
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    resolve(response.getReturnValue());
                } else if (state === "ERROR") {
                    //reject(new Error(response.getError()));
                    let errors = response.getError();
                    reject(new Error(errors && Array.isArray(errors) && errors.length === 1 ? errors[0].message : JSON.stringify(errors)));
                }
            });
            $A.enqueueAction(action);
        });
    },
})