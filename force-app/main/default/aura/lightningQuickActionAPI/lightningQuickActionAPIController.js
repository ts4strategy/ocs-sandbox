({
	 updateCompanyStatus : function( cmp, event, helper ) {
        var actionAPI = cmp.find("quickActionAPI");
        var args = {actionName: "Opportunity.createContact", entityName: "Opportunity"};
        actionAPI.setActionFieldValues(args).then(function(){
            actionAPI.invokeAction(args);
        }).catch(function(e){
            console.error(e.errors); 
        });
         
    }
})