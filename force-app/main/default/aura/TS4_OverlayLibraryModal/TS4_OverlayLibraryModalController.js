({
    handleCancel : function(component, event, helper) {
        //closes the modal or popover from the component
        var appEvent = $A.get("e.c:TS4_OverlayLibraryModalEvent");
        appEvent.setParams({
            "message" : "Cancel", 
            "buttonName" :  component.get("v.buttonName") });
        appEvent.fire();
        component.find("overlayLib").notifyClose();
    },
    handleOK : function(component, event, helper) {
        //do something
        var appEvent = $A.get("e.c:TS4_OverlayLibraryModalEvent");
        appEvent.setParams({
            "message" : "Ok",
            "buttonName" :  component.get("v.buttonName") });
        appEvent.fire();
        component.find("overlayLib").notifyClose();
    }
})