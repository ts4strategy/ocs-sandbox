({
   openModel: function(component, event, helper) {
      // Set isModalOpen attribute to true
      component.set("v.isModalOpen", true);
   },
  
   closeModel: function(component, event, helper) {
      // Set isModalOpen attribute to false  
      component.set("v.isModalOpen", false);
   },
  
   submitDetails: function(component, event, helper) {
      // Set isModalOpen attribute to false
      //Add your code to call apex method or do some processing
      var dd = component.get("v.account");
       var con = component.get("v.contact");
       
       
       const createContact = component.get("c.createContact");
       createContact.setParams({"con" : con});
       
       helper.promesa(component, createContact)
       .then($A.getCallback(function(response){
           console.log("Contacto creado");
       }))
       
       component.set("v.isModalOpen", false);

       
   },
})